#[macro_use]
extern crate clap;

use anyhow::{anyhow, Result};
use clap::App;

mod rtask;

fn main() -> Result<()> {
    let yaml = load_yaml!("../args.yml");
    let flags = App::from_yaml(yaml).get_matches();

    let editor = match std::env::var("EDITOR") {
        Ok(editor) => editor,
        Err(_) => return Err(anyhow!("Please set your $EDITOR")),
    };
    match flags.subcommand() {
        ("init", _) => rtask::init()?,
        ("new", Some(x)) => {
            rtask::check_init()?;
            rtask::new(
                x.value_of("name").unwrap(),
                x.value_of("date").unwrap(),
                x.value_of("content").unwrap(),
            )?
        }
        ("edit", Some(x)) => {
            rtask::check_init()?;
            rtask::edit(x.value_of("name").unwrap(), &editor)?
        }
        ("done", Some(x)) => {
            rtask::check_init()?;
            rtask::done(x.value_of("name").unwrap())?
        }
        ("detail", Some(x)) => {
            rtask::check_init()?;
            rtask::detail(x.value_of("name").unwrap())?
        }
        _ => {
            rtask::check_init()?;
            rtask::show_all()?
        }
    };
    Ok(())
}
