use anyhow::{anyhow, Result};
use std::fs::File;

use chrono::{
    offset::{TimeZone, Utc},
    NaiveDateTime,
};

use std::{
    env, fs,
    io::{BufRead, BufReader, Write},
    path::Path,
    process::Command,
};

macro_rules! zip {
    ($x: expr) => ($x);
    ($x: expr, $($y: expr), +) => (
        $x.iter().zip(
            zip!($($y), +))
    )
}

const DATE_FMT: &str = "%d/%m/%Y/%H:%M";

pub fn check_init() -> Result<()> {
    let root = root_path()?;
    if !Path::new(&root).exists() {
        Err(anyhow!(
            "You have to initiate program before use:\nrtask init"
        ))
    } else {
        Ok(())
    }
}

fn root_path() -> Result<String> {
    let home = env::var("HOME")?;
    Ok(format!("{}/.rtask", home))
}

fn init_aux(root_path: &str) -> Result<()> {
    fs::create_dir_all(root_path)?;
    println!("rtask initialized.\nTo see help page run rtask with flag --help");
    Ok(())
}

pub fn init() -> Result<()> {
    init_aux(&root_path()?)
}

pub fn done(task_name: &str) -> Result<()> {
    let new_filename = format!("{}/.{}.rtask", root_path()?, task_name);
    let old_filename = format!("{}/{}.rtask", root_path()?, task_name);

    if !Path::new(&old_filename).exists() {
        return Err(anyhow!("Task not found."));
    }

    fs::rename(old_filename, new_filename)?;
    Ok(())
}

pub fn new(name: &str, date: &str, content: &str) -> Result<()> {
    let task_path = format!("{}/{}.rtask", root_path()?, name);
    if Path::new(&task_path).exists() {
        return Err(anyhow!("Task with the same name already exists."));
    }
    match NaiveDateTime::parse_from_str(date, DATE_FMT) {
        Ok(_) => (),
        Err(_) => return Err(anyhow!("Date format is dd/mm/YYYY/HH:MM")),
    }
    let mut file = fs::File::create(task_path)?;
    file.write_all(format!("{}\n{}", date, content).as_bytes())?;
    Ok(())
}

pub fn detail(task_name: &str) -> Result<()> {
    let task_path = format!("{}/{}.rtask", root_path()?, task_name);

    if !Path::new(&task_path).exists() {
        return Err(anyhow!("There is no task with the name {}", task_name));
    }

    if let Ok(file) = File::open(task_path) {
        for (index, line) in BufReader::new(file).lines().enumerate() {
            if index == 0 {
                let date = line?.clone();
                println!("{} {}", task_name, date);
            } else {
                println!("{}", line?);
            }
        }
    }

    Ok(())
}

pub fn edit(task_name: &str, editor: &str) -> Result<()> {
    let filename = format!("{}/{}.rtask", root_path()?, task_name);
    if !Path::new(&filename).exists() {
        return Err(anyhow!("Task not found."));
    }
    Command::new(editor).arg(&filename).status()?;
    Ok(())
}

pub fn show_all() -> Result<()> {
    let root = root_path()?;
    let mut dates: Vec<String> = Vec::new();
    let mut names: Vec<String> = Vec::new();
    for entry in fs::read_dir(root)? {
        let entry = entry?;
        if entry.metadata()?.is_file() {
            let mut filename: String = entry.file_name().to_str().unwrap().into();
            if !filename.starts_with('.') {
                names.push(filename.drain(..filename.len() - 6).collect());
                let file = fs::File::open(entry.path())?;
                dates.push(BufReader::new(file).lines().next().unwrap()?);
            }
        }
    }

    let now = Utc::now();
    let utc = Utc;
    let mut time_left: Vec<String> = Vec::new();
    for date in &dates {
        let date = utc.datetime_from_str(&date, DATE_FMT)?;
        let dt = date - now;
        time_left.push(format!(
            "{}d {}h",
            dt.num_days(),
            dt.num_hours() - (dt.num_days() * 24)
        ));
    }
    println!("{0: <20} {1: <20} {2: <20}", "NAME", "DATE", "TIME LEFT");
    println!();
    let z = zip!(names, dates, time_left);
    for (name, (date, time)) in z {
        println!("{0: <20} {1: <20} {2: <20}", name, date, time);
    }

    Ok(())
}
