# rtask

![](https://img.shields.io/badge/License-GPLv3-blue)

Ultimate task manager that can also make coffee

## Dependencies

- [clap](https://crates.io/crates/clap)
- [chrono](https://crates.io/crates/chrono)
- [anyhow](https://crates.io/crates/anyhow)

## Help

```
rtasks 1.0
Ultimate task manager that can also make coffee

USAGE:
    rtask [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    detail    Display details about a task
    done      Mark a task as done
    edit      Edit a task
    help      Prints this message or the help of the given subcommand(s)
    init      Initialize
    new       Create a new task

```
